public class Test{
    public static void main(String []args){
        Invitation one = new Invitation("bla", "name");
        Invitation two = new Invitation("blaz");
    }
}
public class Invitation{
    private String hostName;
    private String address;
    public Invitation(String n, String a){
        hostName = n;
        address = a;
    }
    public Invitation(String a){
        hostName = "Host";
        address = a;
    }
    public String getName(){
        return hostName;
    }
    public void addrUpdate(String addr){
        address = addr;
    }
    public String partyInv(String name){
        return "Dear " + name + ", please attend my event at " + address + ". See you then, " + hostName + ".";
    }

}