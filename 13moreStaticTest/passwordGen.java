import java.util.Random;
import java.lang.Math;
public class Test{
    public static void main(String []args){
        PasswordGenerator one = new PasswordGenerator(4, "bru");
        System.out.println(one.pwCount());
        System.out.println(one.pwGen());
        System.out.println(one.pwCount());
    }
}
public class PasswordGenerator{
    private static int count = 0;
    private String password;
    private String prefix;
    private int length;
    public PasswordGenerator(int len, String pre){
        prefix = pre;
        length = len;
    }
    public PasswordGenerator(int len){
        prefix = "A";
        length = len;
    }
    public int pwCount(){
        return count;
    }
    public String pwGen(){
        count++;
        Random rnd = new Random();
        int n = (int) Math.pow(10, length);
        password = prefix + "." + rnd.nextInt(n);
        return password;
    }
}