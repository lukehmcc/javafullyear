public class Iterator{
	public static void main(String []args){	
		AdditionPattern plus3 = new AdditionPattern(2, 3);
        System.out.println(plus3.currentNumber());
        plus3.next();	
        System.out.println(plus3.currentNumber());
        plus3.next();	 
        plus3.next();	 
        System.out.println(plus3.currentNumber());
        plus3.prev();	
        plus3.prev();	
        plus3.prev();
        System.out.println(plus3.currentNumber());
        plus3.prev();
        System.out.println(plus3.currentNumber());
	}
}
public class AdditionPattern{
    private int curr;
    private int start;
    private int interval;
    public AdditionPattern(int s, int i){
        curr = s;
        start = s;
        interval = i;
    }
    public int currentNumber(){
        return curr;
    }
    public int next(){
        curr += interval;
        return curr;
    }
    public int prev(){
        if(curr-interval >= start){
            curr -= interval;
            return curr;
        }else{
            return curr;
        }
    }
}