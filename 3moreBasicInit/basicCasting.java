public class Init{
    public static void main(String args[]){
        int pieces = 8;
        double caloriePerSlice = 356.75;
        double totalCalories = (double) (pieces * caloriePerSlice);
        System.out.println("A whole pizza has " + totalCalories + " Calories.");
    }
}