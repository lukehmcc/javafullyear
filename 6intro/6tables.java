public class tables{
	public static void main(String []args){	
        double oneDeskWidth = 7;  
		double oneDeskLength = 11;
		double totalWidth = 114;
		double totalLength = 200; 
		double oneArea = oneDeskLength * oneDeskWidth;
		double totalArea = totalLength * totalWidth;
		int amntDesks = (int) (totalArea / oneArea);
		System.out.println("You could fit " + amntDesks + " desks.");
	}
}
