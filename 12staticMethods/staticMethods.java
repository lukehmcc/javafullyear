import java.util.Scanner;

public class RockPaperScissors
{
    private static final String USER_PLAYER = "User wins!";
    private static final String COMPUTER_PLAYER = "Computer wins!";
    private static final String TIE = "Tie";
        
    public static boolean getWinner(String user, String computer)
    {
        System.out.println("User: " + user);
        System.out.println("Computer: " + computer);
        if(user.equals(computer)){
            System.out.println(TIE);
            return false;
        } else if(user.equals("rock") && computer.equals("paper")){
            System.out.println(COMPUTER_PLAYER);
        } else if(user.equals("scissors") && computer.equals("paper")){
            System.out.println(USER_PLAYER);
        } else if(user.equals("rock") && computer.equals("scissors")){
            System.out.println(USER_PLAYER);
        } else if(user.equals("scissors") && computer.equals("rock")){
            System.out.println(COMPUTER_PLAYER);
        } else if(user.equals("paper") && computer.equals("rock")){
            System.out.println(USER_PLAYER);
        } else if(user.equals("paper") && computer.equals("scissors")){
            System.out.println(COMPUTER_PLAYER);
        }
        return true;
    }
    
    public static void main(String[] args)
    {
        Randomizer rand = new Randomizer();
        Scanner input = new Scanner(System.in);
        boolean winner = false;
        while(winner == false){
            System.out.println("Enter your choice (rock, paper, or scissors):");
            String userIn = input.nextLine();
            int random = rand.nextInt(1, 3);
            if(random == 1){
                winner = getWinner(userIn, "rock");
            } else if(random == 2){
                winner = getWinner(userIn, "paper");
            } else if(random == 3){
                winner = getWinner(userIn, "scissors");
            }
            
        }
        System.out.println("Thank you for playing!");   
    }
}

public class Randomizer
{
    public static int nextInt()
    {
        //Implement this method to return a random number from 1-10
        return (int)(Math.random() * (10-1 + 1) + 1);
    }
    
    public static int nextInt(int min, int max)
    {
        //Implement this method to return a random integer between min and max
        return (int)(Math.random() * (max - min + 1) + min);
        
    }
}
