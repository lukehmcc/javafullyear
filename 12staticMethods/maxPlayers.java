public class PlayerTester
{
    public static void main(String[] args)
    {
        Player one = new Player();

        System.out.println(one.gameFull());
    }
}


public class Player 
{
    public static int totalPlayers = 0;
    public static int maxPlayers = 10;
    
    // Public Methods
    public Player() 
    {
        totalPlayers++;
    }
    
    // Static Methods 
    public static boolean gameFull(){
        System.out.println(totalPlayers);
        System.out.println(maxPlayers);
        if (totalPlayers > maxPlayers){
            return true;
        }
        return false;
    }
}
