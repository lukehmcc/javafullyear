public class Test{
	public static void main(String []args){	
		String reversed = reverseString("ABCDE");
		System.out.println(reversed);
        palindromeChecker("taco cat");
	}
	public static String reverseString(String str){
		String result = "";
		for(int i = str.length()-1; i >= 0; i--){
			result += str.charAt(i);
		}
		return result;
	}
    public static String removeSpaces(String str){
        return str.replaceAll("\\s", "");
    }
    public static void palindromeChecker(String str){
        str = str.toLowerCase();
        str = removeSpaces(str);
        boolean checker = true;
        if(str.equals(reverseString(str))){
            checker = true;
        }else{
            checker = false;
        }
        System.out.println(checker);
    }
}
