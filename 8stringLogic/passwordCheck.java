import java.util.Scanner;
public class Password
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        // Prompt the user to enter their password and pass their string
        // to the passwordCheck method to determine if it is valid.
        System.out.println("Password please:");
        String password = input.nextLine();
        boolean pass = passwordCheck(password);
        System.out.println(pass);
    }
    
    public static boolean passwordCheck(String password){
        // Create this method so that it checks to see that the password
        // is at least 8 characters long and only contains letters 
        // and numbers.
        String index = "abcdefghijklmnopqrstuvwxyz1234567890";
        boolean passed = true;
        if(password.length() < 8){
            return false;
        }
        for(int i = 0; i < password.length(); i++){
            boolean contain = false;
            for(int c = 0; c < index.length(); c++){
                if(password.substring(i, i+1).equals(index.substring(c, c+1))){
                    contain = true; 
                }
            }
            if(contain == false){
                return false;
            }
        }
        return true;
    }
}
