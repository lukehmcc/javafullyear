import java.util.Scanner;
public class Grammar
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Gimme a sentence:");
        String sentence = input.nextLine();
        String properSentence = useProperGrammar(sentence);
        System.out.println(properSentence);
    }
    
    public static String useProperGrammar(String theText)
    {
        String proper = "";
        int count = 0;
        for(int i = 0; i < theText.length(); i++){
            String now = theText.substring(i, i+1); 
            if(now.equals("2")){
                proper += "to";
                count += 1;
            }else{
                proper += now;
            }
        }
        System.out.println("Fixed " + count + " grammatical errors:");
        return proper;
    }
}