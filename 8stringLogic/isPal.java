import java.util.Scanner;
public class Palindromes
{
    /**
     * This program lets the user input some text and
     * prints out whether or not that text is a palindrome.
     */
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Gimme a word:");
       // Create user input and let user know whether their word is a palindrome or not! 
        String word1 = input.nextLine();
        boolean isAPal = isPalindrome(word1);
        System.out.println(isAPal);
    }
    
    public static boolean isPalindrome(String text)
    {
       String word2 = reverse(text);
       if(text.equals(word2)){
           return true; 
       }else{
           return false;
       }
    }
    
    public static String reverse(String text)
    {
        String word3 = "";
        for(int i = 0; i < text.length(); i++){
            word3 += text.substring((text.length() - i -1), (text.length() - i));
        }
        return word3; 
    }

}