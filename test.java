public class Fibonacci 
{
    public static void main(String[] args) 
    {
      
        //number of elements to generate in the sequence
		int max = 15;
		
		// create the array to hold the sequence of Fibonacci numbers
		int[] sequence = {};
		
		//create the first 2 Fibonacci sequence elements
		sequence[0] = 0;
		sequence[1] = 1;
        
        System.out.println("Fibonacci sequence up to 15 terms: " + findFib(sequence, 15));
        
        System.out.println("\nIndex position of 55 is: " + findIndex(sequence, 55, 15));
    
    }
      
    // This method finds the index of an element in an array 
      
    public static int findFib (int[] arr, int n) 
    {
        for(int i = 0; i < arr.length; i++){
            System.out.print(arr[i]);
        }
        for(int i = arr.length; i < n; i++){
            arr.add(arr[i-1] + arr[i-2]);
            System.out.print(arr[i]);
        }
    }
    public static int findIndex (int[] arr, int n, int f) 
    {   
        for(int i = 0; i < arr.length; i++){
            if(arr[i] == f){
                return i;
            }
        }
        for(int i = arr.length; i < n; i++){
            arr.add(arr[i-1] + arr[i-2]);
            if(arr[i] == f){
                return i;
            }
        }
        return -1;
    }
}