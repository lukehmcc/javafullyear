public class NumberTriangle
{
    public static void main(String[] args)
    {
       // Call makeNumberTriangle 
       makeNumberTriangle();
    }
    
    // Makes an upright triangle with the numbers 1-5
    public static void makeNumberTriangle()
    {
        // Your code goes here! 
        for(int i = 0; i < 5; i++){
            for(int c = 0; c <= i; c++){
                System.out.print(c);
            }
        System.out.println();
        }
    }
}
