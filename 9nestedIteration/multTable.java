public class MultiplicationTable
{
    public static void main(String[] args)
    {
        // Call makeMultiplicationTable 
       makeMultiplicationTable();
    }
    
    // Makes a multiplcation table
    public static void makeMultiplicationTable()
    {
        for(int i = 1; i <= 10; i++){
            for(int c = 1; c <= 10; c++){
                System.out.print((i * c) + "\t");
            }
        System.out.println();
        }
    }
}
