public class TreeOfStars
{
    public static void main(String[] args)
    {
       // Call makeATree 
       makeATree();
    }
    public static void makeATree()
    {
        int iterator = 1; 
        for(int i = 8; i >= 0; i--){
            for(int c = 0; c <= (10 - iterator); c++){
                System.out.print(" ");
            }
            for(int c = 2; c <= (iterator + 1); c++){
                System.out.print("* ");
            }
        System.out.println();
        iterator++;
        }
    }
}
